# Azure patches

Azure patches is a folder containing specific line-by-line patches that may have to be applied on top of official repos. This is to prevent creating extra alternative repos that may complicate future merger efforts

Current patches available includes:

| Component         | Version   | Patch File                      | Functionality                      |
|-------------------|-----------|---------------------------------|------------------------------------|
| tosca             | 99ecf26   | tosca.99ecf26.patch             | Replaces URL for directory listing |
| hysds-dockerfiles | `python2` | hysds-dockerfiles.python2.patch | Uses legacy `hysdsazure/verdi` Docker image for Verdi |

**Caution**: These patch files are based on partial diffs and must be changed if the target file differs! (Such as when the target repo has been updated to a newer version)